from flask import Flask, request, render_template
from random import randint

app = Flask(__name__)

dice_sides = ['4','6','8','10','12','20']

@app.route('/')
@app.route('/index')
def index():
    return render_template('selection.html', option_list=dice_sides, sides=dice_sides[0])

@app.route('/roll', methods=['GET', 'POST'])
def roll():
    if request.method == 'POST':
        sides = int(request.form.get('sides'))
        roll = randint(1, sides)
        return render_template('selection.html', option_list=dice_sides, sides=str(sides), roll=str(roll))
            
    return render_template('selection.html', option_list=dice_sides, sides=dice_sides[0])

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
